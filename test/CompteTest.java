import org.junit.Test;
import static org.junit.Assert.*;

public class CompteTest {
    @Test
    public void testCreationCompte() throws UnsignedNumberException {
        Compte c1 = new Compte();
        assertTrue(c1.getSolde() == 0);
        Compte c2 = new Compte(1000);
        assertTrue(c2.getSolde() == 1000);

    }

    @Test(expected = UnsignedNumberException.class)
    public void testCreationCompteNegatif() throws UnsignedNumberException {
        Compte c2 = new Compte(-10);
    }

    @Test
    public void testCredit() throws UnsignedNumberException{
        int i1 = 500;
        Compte c1 = new Compte(1500);
        c1.credit(i1);
        assertTrue(c1.getSolde() == 2000);
    }

    @Test(expected = UnsignedNumberException.class)
    public void testCreditNegatif() throws UnsignedNumberException{
        int i1 = -500;
        Compte c1 = new Compte(1500);
        c1.credit(i1);
    }

    @Test
    public void testDebit() throws UnsignedNumberException, BalanceExceededException {
        Compte c1 = new Compte(1000);
        c1.debit(100);
        assertTrue(c1.getSolde() == 900);
    }

    @Test(expected = UnsignedNumberException.class)
    public void testDebitNegatif() throws UnsignedNumberException, BalanceExceededException{
        Compte c1 = new Compte(1000);
        c1.debit(-10);
    }

    @Test(expected = UnsignedNumberException.class)
    public void testDebitDepassantSolde() throws UnsignedNumberException, BalanceExceededException{
        Compte c1 = new Compte(1000);
        c1.debit(-10);
        c1.debit(1000);
    }

    @Test
    public void testVirement() throws UnsignedNumberException, BalanceExceededException {
        Compte c1 = new Compte(199000);
        Compte c2 = new Compte(35000);
        c1.virement(c2,35000);
        assertTrue(c1.getSolde() == 164000);
        assertTrue(c2.getSolde() == 70000);
    }

    @Test(expected = UnsignedNumberException.class)
    public void testVirementNegatif() throws UnsignedNumberException, BalanceExceededException{
        Compte c1 = new Compte(199000);
        Compte c2 = new Compte(35000);
        c1.virement(c2,-35000);
    }

    @Test(expected = BalanceExceededException.class)
    public void testVirementDepassantSolde() throws UnsignedNumberException, BalanceExceededException{
        Compte c1 = new Compte(199000);
        Compte c2 = new Compte(35000);
        c1.virement(c2,235000);
    }
}